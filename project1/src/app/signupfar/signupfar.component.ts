import { Component } from '@angular/core';
import { projectService } from '../project.service';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-signupfar',
  templateUrl: './signupfar.component.html',
  styleUrls: ['./signupfar.component.css']
})
export class SignupfarComponent {
  registrationForm: FormGroup;
  constructor(private service :projectService){
    this.registrationForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl('', [Validators.required]),
    });

  }
  registerfarmer(farmer: any) {

  }

  onSubmit(farmer:any) {
    if (this.registrationForm.valid) {
      this.service.registerfarmer(farmer).subscribe();
      console.log(this.registrationForm.value);
    } else {
     alert('please register to proceed');
      console.log("Form is invalid");
    }
  }
  minlengthValidator(minlength: number) {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value && control.value.length < minlength) {
        return { minlength: true };
      }
      return null;
    };
  }
}
