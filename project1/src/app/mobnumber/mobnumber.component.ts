import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { projectService } from '../project.service';

@Component({
  selector: 'app-mobnumber',
  templateUrl: './mobnumber.component.html',
  styleUrls: ['./mobnumber.component.css'],
})

export class MobnumberComponent implements OnInit {
  flag: boolean = true;

  constructor(private router: Router, private service: projectService) {}

  ngOnInit(): void {
    //throw new Error('Method not implemented.');
  }

  // checking for valid mobile number and navigating to the next page if its correct
  mob(mobForm: any) {
  //   this.service.setMobile(mobForm.mobile);
  //   this.service.getOtp(mobForm.mobile).subscribe((data1: any) => {
  //     this.service.setData(data1);
  //     if (data1 != 0) {
  //       this.router.navigate(['takeotp']);
  //     } else {
  //       let msz = <HTMLElement>document.getElementById('popmsz');
  //       msz.innerHTML = 'Invalid Mobile Number';
  //     }
  //   });
  
  this.service.allcustomers.forEach((customer:any) => {
    if(mobForm.mobile==customer.mobile_Number){
      this.flag=false;
      this.service.resetnumber=mobForm.mobile;
      this.service.sendOtpBySms(mobForm.mobile).subscribe((data:any) => {
        this.service.otp=data;
        if(data != 0){
          this.router.navigate(['login/forgotpassword/takemail/takeotp']);
        alert('Otp received')}
      })
    }
  })

  this.service.allfarmers.forEach((farmer:any) => {
    if(mobForm.mobile==farmer.mobile_Number){
      this.flag=false;
      this.service.flag=false;
      this.service.resetnumber=mobForm.mobile;
      this.service.sendOtpBySms(mobForm.mobile).subscribe((data:any) => {
        this.service.otp=data;
        if(data != 0){
           this.router.navigate(['login/forgotpassword/takemail/takeotp']);
          alert('Otp received')
        }
      })
    }
  })

  if(this.flag){
    let msz = <HTMLElement>document.getElementById('popmsz');
      msz.innerHTML = 'Invalid Email-Address';
  }
}
  
}
