import { Component, OnInit } from '@angular/core';
import { projectService } from '../project.service';
import { Router } from '@angular/router';
import { OidcSecurityService } from 'angular-auth-oidc-client';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  customers: any;
  farmers: any;
  flag: any;
  isAuthenticated: boolean = false;



  constructor(private service: projectService, private router: Router,private oidcSecurityservice: OidcSecurityService

    ) {
    this.customers;
    this.farmers;
    this.flag = false;
  }
  ngOnInit(): void {
    this.oidcSecurityservice.isAuthenticated$.subscribe(({ isAuthenticated }) => {
      this.isAuthenticated = isAuthenticated;
    })


    this.service.getcustomers().subscribe((data: any) => {
      this.customers = data;
      this.service.allcustomers=data;
      console.log('Recieved customers data in login', this.customers)
    });
    this.service.getfarmers().subscribe((data: any) => {
      console.log('Recieved customers data in login', data)
      this.farmers = data;
      this.service.allfarmers=data;

    });
  }

 async loginsubmit(loginForm: any) {
  console.log(loginForm.email_Id); 
    await this.service.famerlogin(loginForm.email_Id, loginForm.password).toPromise().then((data: any) => {
      this.service.selctedfarmer = data;
    });

    await this.service.customerlogin(loginForm.email_Id, loginForm.password).toPromise().then((data: any) => {
      this.service.selectedcustomer = data;
    });

    if (loginForm.email_Id == "admin" && loginForm.password == "admin") {
      this.flag = true;
      alert("Welcome to Admin log");
      this.router.navigate(['admin'])
    } else if (this.service.selctedfarmer != null) {
      alert('WELCOME TO farmer HOME PAGE');
      this.router.navigate(['farmhomepage']);
    } else if (this.service.selectedcustomer != null) {
      alert('WELCOME TO customer HOME PAGE');
      this.router.navigate(['customerhomepage']);
    } else {
      alert("Invalid details");
    }
}

googleLogin() {
  this.oidcSecurityservice.authorize();
  // if (this.isAuthenticated) {
  //    //this.service.setUserLogin();
  //   this.router.navigate(['customerhomepage']);
  // }
}


}

