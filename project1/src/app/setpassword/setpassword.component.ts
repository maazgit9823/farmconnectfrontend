import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { projectService } from '../project.service';

@Component({
  selector: 'app-setpassword',
  templateUrl: './setpassword.component.html',
  styleUrls: ['./setpassword.component.css'],
})

export class SetpasswordComponent implements OnInit {

  setPass: any;

  constructor(private router: Router, private service: projectService) {
    this.setPass = {
      mobile_Number: '',
      password: '',
    };
  }

  ngOnInit(): void {
   // throw new Error('Method not implemented.');
  }

  // method for setting password
  pass(setForm: any) {
    // this.setPass.phoneNo = this.service.getMobile();

    // this.setPass.password = setForm.pword;
    // this.service.updatePass(this.setPass).subscribe((data1: any) => {
    //   if (data1 != 0) {
    //     this.router.navigate(['/']);
    //   } else {
    //     let msz = <HTMLElement>document.getElementById('popmsz');
    //     msz.innerHTML = 'Failed to change password';
    //   }
    // });
    this.setPass.mobile_Number=this.service.resetnumber;
    console.log(this.service.resetnumber)
    this.setPass.password=setForm.password;
    console.log(setForm.password)
   if(this.service.flag){
      this.service.UpadtePassMobC(this.setPass).subscribe((data:any) => {
        console.log(data)
        if(data!=0){
          let msz = <HTMLElement>document.getElementById('popmsz');
         msz.innerHTML = 'Password Changed Successfully';
          this.router.navigate(['/login']);
        }
      })
    }
    if(!this.service.flag){
      this.service.UpadtePassMobF(this.setPass).subscribe((data:any) => {
        console.log(data)
        if(data!=0){
          let msz = <HTMLElement>document.getElementById('popmsz');
         msz.innerHTML = 'Password Changed Successfully';
          this.router.navigate(['/login']);
        }
      })
    }
  }
  
}
