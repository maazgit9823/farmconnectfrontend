import { NgModule } from '@angular/core';
import { AuthModule } from 'angular-auth-oidc-client';
@NgModule({
    imports: [AuthModule.forRoot({
        config: {
            authority: 'https://dev-gatqv80djj2g4l4j.us.auth0.com',
            redirectUrl: 'http://localhost:4200',
            postLogoutRedirectUri: 'http://localhost:4200',
             postLoginRoute:'/customerhomepage',
            // postLoginRoute: window.location.origin,
            clientId: 'fu80Ko2CBi4DlHZP3Q3w7tyRUNPMMjre',
            scope: 'openid profile offline_access',
            responseType: 'code',
            silentRenew: true,
            useRefreshToken: true,
        }
      })],
    exports: [AuthModule],
})
export class AuthConfigModule {}
