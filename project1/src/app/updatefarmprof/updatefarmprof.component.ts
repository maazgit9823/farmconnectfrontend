import { Component } from '@angular/core';
import { projectService } from '../project.service';

@Component({
  selector: 'app-updatefarmprof',
  templateUrl: './updatefarmprof.component.html',
  styleUrls: ['./updatefarmprof.component.css']
})
export class UpdatefarmprofComponent {
  farmer:any;
  editfarmer:any;
  name:any;
  email:any;
  mobile:any;
  address:any;
  constructor(private service :projectService){
    this.farmer = this.service.selctedfarmer;
    console.log(this.farmer);
    this.editfarmer = {farmer_Name: '',mobile_Number: '', farmer_Address:''};
   }
   showedit(){
    this.editfarmer = this.farmer;
   }
   updatefarmer(farmer:any){
   
    this.service.updatefarmer(this.editfarmer).subscribe();

  }
}
