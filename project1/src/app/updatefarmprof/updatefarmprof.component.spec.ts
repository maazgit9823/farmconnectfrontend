import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatefarmprofComponent } from './updatefarmprof.component';

describe('UpdatefarmprofComponent', () => {
  let component: UpdatefarmprofComponent;
  let fixture: ComponentFixture<UpdatefarmprofComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UpdatefarmprofComponent]
    });
    fixture = TestBed.createComponent(UpdatefarmprofComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
