import { Component } from '@angular/core';
import { projectService } from '../project.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {
  customers:any;
  farmers:any;
  products:any;
  // constructor(private renderer: Renderer2) { }
  // private renderer: Renderer2, private elementRef: ElementRef
  constructor(private service: projectService){
    this.service.getcustomers().subscribe((data: any)=>{
      console.log(data);
      this.customers = data;})
  
}
}
