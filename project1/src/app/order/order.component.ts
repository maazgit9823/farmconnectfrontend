import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ElementRef,ViewChild } from '@angular/core';
import { projectService } from '../project.service';
import { data } from 'jquery';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./style.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class OrderComponent implements OnInit {
 orders:any;
  constructor(private service: projectService){
  }
  ngOnInit() {
this.orders= this.service.orderitems;
  console.log(this.orders);
  }

// getorders(order:any){
//  this.service.getorderbycustomerId(order).subscribe((data)=>{
// console.log(data);
// this.orders = data;})
// console.log(this.orders);
// }
  @ViewChild('orderButton')
  orderButton!: ElementRef;
  onClick(){
  const button = this.orderButton.nativeElement;
if(!button.classList.contains('animate')){
button.classList.add('animate');
setTimeout(() => {
button.classList.remove('animate');
}, 10000);
    }
  }
}
