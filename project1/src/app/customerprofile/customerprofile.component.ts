import { Component } from '@angular/core';
import { projectService } from '../project.service';
import { data } from 'jquery';

@Component({
  selector: 'app-customerprofile',
  templateUrl: './customerprofile.component.html',
  styleUrls: ['./customerprofile.component.css']
})
export class CustomerprofileComponent {
  customer:any;
  editcustomer:any;
  constructor(private service :projectService){
    this.customer = this.service.selectedcustomer;
    this.editcustomer = {customer_Name: '',mobile_Number: '', customer_address:'',email_Id:''};
   }
   showedit(customer:any){
    this.editcustomer =this.customer;
   }
   updatecustomer(customer:any){
    this.service.updatecustomer(this.editcustomer).subscribe(data);
   }
}
