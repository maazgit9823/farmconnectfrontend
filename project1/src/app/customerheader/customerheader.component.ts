import { Component, OnInit } from '@angular/core';
import { projectService } from '../project.service';
import { Router } from '@angular/router';
import { OidcSecurityService } from 'angular-auth-oidc-client';


@Component({
  selector: 'app-customerheader',
  templateUrl: './customerheader.component.html',
  styleUrls: ['./customerheader.component.css']
})
export class CustomerheaderComponent implements OnInit{
constructor(private router: Router,private service : projectService,private oidcSecurityservice: OidcSecurityService){
  
}
  ngOnInit(): void {
    
  }
//Dont remove it
 logout() {
  this.oidcSecurityservice.logoff().subscribe(() => {
    localStorage.clear();
    sessionStorage.clear();
  });
}
profile(){
  this.router.navigate(["/customerprofile"]);
}
updateprofile(){
  this.router.navigate(["/updateprofile"]);
}

}
