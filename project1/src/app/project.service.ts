import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs/internal/Observable';

function _window() : any {
  return window;
}

@Injectable({
  providedIn: 'root',
})
export class projectService {
  get nativeWindow() : any {
    return _window();
 }
  removeToCart(cartId: any) {
    return this.httpclient.delete('deleteFromCart/' + cartId);
  }
  isUserLogged: boolean;
  selectedcustomer: any;
  selctedfarmer: any;
  selectedproducts: any;
  cartitems: any;
  orderdata:any;
  value:number |any;
  farmerproduct:any;
  orderitems:any;
  googleauth:any;
  encodedPassword: any;
  datapass:any;
  allfarmers:any;
  allcustomers:any;
  resetemail:any;
  resetnumber:any;
  otp:any;
  flag:boolean;
  constructor(private httpclient : HttpClient) {
    this.selectedcustomer;
    this.selctedfarmer;
    this.isUserLogged = false;
    this.selectedproducts=[];
    this.cartitems=[];
  this.farmerproduct=[];
  this.orderitems=[];

  
  
    this.selectedcustomer;
    this.selctedfarmer;
this.isUserLogged=false;
this.googleauth=false;
this.encodedPassword;
this.datapass;
this.allcustomers;
this.allfarmers;
this.resetemail;
this.resetnumber;
this.otp;
this.flag=true;
   }
  isUserLoggedIn() {
    this.isUserLogged = true;
  }

  isUserLoggedOut() {
    return false;
  }
  getUserLogged() {
    return this.isUserLogged;
  }
  getcustomers() {
    return this.httpclient.get('showAllCustomer');
  }
  getfarmers() {
    return this.httpclient.get('showAllFarmer');
  }
  registerCustomer(customers: any) {
    return this.httpclient.post('registerCustomer', customers);
  }
  registercustomer(customer: any) {
    return this.httpclient.post('registerCustomer', customer);
  }
  registerfarmer(farmer: any) {
    return this.httpclient.post('registerFarmer', farmer);
  }
  deletecustomer(customer_Id: any) {
    return this.httpclient.delete('deleteCustomer/' + customer_Id);
  }
  deletefarmer(farmer_Id: any) {
    return this.httpclient.delete('deleteFarmer/' + farmer_Id);
  }
  updatecustomer(editObject: any) {
    return this.httpclient.put('updateCustomer', editObject);
  }
  updatefarmer(editObject: any) {
    return this.httpclient.put('updateFarmer', editObject);
  }
  getAllproducts() {
    return this.httpclient.get('showAllProducts');
  }
  getProductById(product_Id: any) {
    return this.httpclient.get('showProductById/' + product_Id);
  }
  deleteProductById(product_Id: any) {
    return this.httpclient.delete('deleteProduct/' + product_Id);
  }
  registerproduct(product: any) {
    console.log(product);
    return this.httpclient.post('addProduct/', product);
  }

  getProductsByFarmerId(farmerid: any) {
    return this.httpclient.get('showProductByFarmerId/' + farmerid);
  }

  addtocart(product: any) {
    return this.httpclient.post('/addtocart/', product);
  }

  getallcartitems() {
    return this.httpclient.get('showAllCartProducts');
  }

  getcartitemsbyid(customer_Id: any) {
    return this.httpclient.get('showCartProductById/' + customer_Id);
  }
  getitems(userid: any) {
    this.getcartitemsbyid(userid).subscribe((result: any) => {
      console.log(result); 
      alert('get items called');
      this.cartitems = result;
      return this.cartitems;
    });
  }

  getcart() {
    return this.getitems;
  }

  //working code.
  // removeItemFromCart(productId: number) {
  //   let cartData = this.cartitems;
  //   if (cartData) {
  //     let items: any[] = JSON.parse(cartData);
  //     items = items.filter((item: any) => productId !== item.id);
  //     localStorage.setItem('localCart', JSON.stringify(items));
  //   }
  // } 
updatequantity(cart:any){
  return this.httpclient.put("upatecart/", cart)
}
removeProduct(product: any) {
  this.removeToCart(product).subscribe();
  const index = this.cartitems.findIndex((x: any) => {
    return x.id === product.id});
  if (index > -2) {
    this.cartitems.splice(index, 1);
  this.cartitems();
  }
}
uploadorders(order:any){
return this.httpclient.post("getorderfromcart",order);
}
getorderbycustomerId(customer:any){
 return this.httpclient.get("getOrderByCustomer_Id/"+ customer);
}

getallorder(){
return  this.httpclient.get("showAllorder");
}
 private valueSubject = new BehaviorSubject<number>(0);
public value$ = this.valueSubject.asObservable();
decreaseValue() {
  const currentValue = this.valueSubject.getValue();
  this.valueSubject.next(currentValue - 1);
}

increaseValue() {
  const currentValue = this.valueSubject.getValue();
  this.valueSubject.next(currentValue + 1);
}


postFile(formData: any) {
  const endpoint = 'savefile';
  console.log("Recieved Upload Data: ",formData)
  return this.httpclient.post(endpoint,formData);
}



customerlogin(emailid: any, password: any): Observable<any> {
  return this.httpclient.get("clogin/" + emailid + "/" + password);
}
famerlogin(emailid:any,password:any){
  return this.httpclient.get("flogin/"+emailid+"/"+password);
}

sendOtpByEmail(emailId:any){
  return this.httpclient.get("/sendOTPbyEmail/" + emailId);
}

sendOtpBySms(number:any){
  return this.httpclient.get("/send-sms/+91"+number);
}

UpadtePassEmailF(obj:any){
  console.log(obj+"in service")
return this.httpclient.put("updatePass",obj)
}
UpadtePassEmailC(obj:any){
  return this.httpclient.put("UpdatePass",obj)
  }

  UpadtePassMobF(setPass: any) {
    return this.httpclient.put("updatePassBysms",setPass);
  }
  UpadtePassMobC(setPass: any) {
    return this.httpclient.put("updatePassBySMS",setPass);
  }

}
