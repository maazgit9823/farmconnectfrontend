import { Component, ViewChild, ElementRef, OnInit} from '@angular/core';
import { projectService } from '../project.service';

declare var jQuery:any;
@Component({
  selector: 'app-farmhomepage',
  templateUrl: './farmhomepage.component.html',
  styleUrls: ['./farmhomepage.component.css']
})
export class FarmhomepageComponent implements OnInit{

@ViewChild('farmerModel', { static: false }) farmerModel: any;
editfarmer:any;
farmer:any;

 farmerId: any;
 products: any;
 allproducts:any;
showUpdateForm: boolean = false;
  
constructor(private service:projectService){
  this.farmer = this.service.selctedfarmer;
  this.editfarmer = {farmer_Name: '', farmer_Address: '', mobile: ''};
  }
  ngOnInit(): void {
    this.service.getProductsByFarmerId(this.farmer.farmer_Id).subscribe((data:any)=>{
      this.products = data;
     })
     this.products=this.service.farmerproduct;
    }

  showEditPopup() {
    this.editfarmer = this.service.selctedfarmer;
    this.farmerModel('show');
  }

  hideEditPopup() {
    this.farmerModel('hide');
  }
}
