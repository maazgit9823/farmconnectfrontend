import { Component, OnInit } from '@angular/core';
import { projectService } from '../project.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-customerhomepage',
  templateUrl: './customerhomepage.component.html',
  styleUrls: ['./customerhomepage.component.css']
})
export class CustomerhomepageComponent implements OnInit {
  
  customer: any;
  allproducts: any;
  Cart: any;
  product: any;
  quantity:any;
  constructor(private service: projectService) {

    // this.Cart={
    //  product_Id:this.product.product_Id,
    //  product_Name:this.product.product_Name,
    //  product_Price:this.product.product_Price,
    //  product_Desc:this.product.product_Price,
    // Customer:{
    //   customer_Id: this.customer.customer_Id,
    //   customer_Name :"",
    //   customer_Address:"",
    //   email_Id:"",
    //   password:"",
    //   mobile_Number:0,
    // }
    // }

    this.product = {
      "cart": 0,
      "product_Desc":"",
      "product_Id": "",
      "product_Name":"",
      "product_Price":0,
      "product_imgsrc":"",
      "quantity":1,
      "customer": {
        "customer_Id": 0,
        "customer_Name": "",
        "customer_address": "",
        "email_Id": "",
        "password": "",
        "mobile_Number": 0
      }

 
      // "Farmer": {
      //   "farmer_Id": 0,
      //   "farmer_Name": "",
      //   "farmer_Address": "",
      //   "email_Id": "",
      //   "mobile_Number": 0
      // }
    }
   
  }
  ngOnInit() {
    this.service.getAllproducts().subscribe((data: any) => {
      this.allproducts = data;

    });
  }
  addtocart(data:any){
  this.service.selectedproducts = data;
  //   console.log('Recived :'+JSON.stringify(data)) 
  //   this.product=[{
  //   ...data,
  // customer_Id:this.service.selectedcustomer,
  //   }];
  this.product = {
    "cart": 0,
    "product_Desc":this.service.selectedproducts.product_Desc,
    "product_Id": this.service.selectedproducts.product_Id,
    "product_Name": this.service.selectedproducts.product_Name,
    "product_Price": this.service.selectedproducts.product_Price,
    "product_imgsrc":this.service.selectedproducts.product_imgsrc,
    "quantity": this.service.selectedproducts.quantity,
    "customer": {
      "customer_Id": this.service.selectedcustomer.customer_Id,
      "customer_Name": "",
      "customer_address": "",
      "email_Id": "",
      "password": "",
      "mobile_Number": 0
    },

  }
  this.service.addtocart(this.product). subscribe((data: any)=>
  {console.warn(data)}),
  console.log(this.product);
this.service.cartitems.push(this.product);  
}
//   addtocart<List>(cart: any) {
// this.service.selectedproducts= cart;
// console.log(this.service.selectedproducts);
//     this.service.addtocart(this.Cart).subscribe((data:any)=>
//     console.warn(data));    
//   }

decreaseValue() {
  this.service.decreaseValue();
}

increaseValue() {
  this.service.increaseValue();
}
}