import { Component } from '@angular/core';
import { projectService } from '../project.service';

@Component({
  selector: 'app-updateprofile',
  templateUrl: './updateprofile.component.html',
  styleUrls: ['./updateprofile.component.css']
})
export class UpdateprofileComponent {
  customer:any;
  editcustomer:any;
  constructor(private service :projectService){
    this.customer = this.service.selectedcustomer;
    this.editcustomer = {customer_Name: '',mobile: '', customer_Address:''};
   }
   showedit(customer:any){
    this.editcustomer =this.customer;
   }
   updatecustomer(customer:any){
    this.service.updatecustomer(this.editcustomer).subscribe();
   }
}
