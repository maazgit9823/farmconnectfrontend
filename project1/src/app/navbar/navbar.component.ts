import { Component } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  modalVisible: boolean = false;
  openModal() {
    this.modalVisible = true;
    console.log("hi this is open");
  }

  closeModal() {
    this.modalVisible = false;
    console.log("hi this is close");
  }
}
