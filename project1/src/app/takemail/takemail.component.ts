import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { projectService } from '../project.service';
@Component({
  selector: 'app-takemail',
  templateUrl: './takemail.component.html',
  styleUrls: ['./takemail.component.css'],
})

export class TakemailComponent implements OnInit {

  flag:any;

  constructor(private router: Router, private service: projectService) {
    this.flag=true;
  }

  ngOnInit(): void {
    //throw new Error('Method not implemented.');
  }

  //method for taking email otp
  mob(eForm: any) {
    this.service.allcustomers.forEach((customer:any) => {
      if(eForm.emailId==customer.email_Id){
        this.flag=false;
        this.service.resetemail=eForm.emailId;
        this.service.sendOtpByEmail(eForm.emailId).subscribe((data:any) => {
          this.service.otp=data;
          if(data != 0){
            this.router.navigate(['login/forgotpassword/takemail/eotp']);
          alert('Otp received')}
        })
      }
    })

    this.service.allfarmers.forEach((farmer:any) => {
      if(eForm.emailId==farmer.email_Id){
        this.flag=false;
        this.service.flag=false;
        this.service.resetemail=eForm.emailId;
        this.service.sendOtpByEmail(eForm.emailId).subscribe((data:any) => {
          this.service.otp=data;
          if(data != 0){
             this.router.navigate(['login/forgotpassword/takemail/eotp']);
            alert('Otp received')
          }
        })
      }
    })

    if(this.flag){
      let msz = <HTMLElement>document.getElementById('popmsz');
        msz.innerHTML = 'Invalid Email-Address';
    }
    // this.service.setMobile(eForm.emailId);
    // this.service.getOtpByE(eForm.emailId).subscribe((data1: any) => {
    //   this.service.setData(data1);
    //   if (data1 != 0) {
    //     this.router.navigate(['eotp']);
    //   } else {
    //     this.flag=false;
    //     let msz = <HTMLElement>document.getElementById('popmsz');
    //     msz.innerHTML = 'Invalid Email-Address';
    //   }
    // });
  }
}
