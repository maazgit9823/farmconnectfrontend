import { Component } from '@angular/core';
import { projectService } from '../project.service';

@Component({
  selector: 'app-finalbill',
  templateUrl: './finalbill.component.html',
  styleUrls: ['./finalbill.component.css']
})
export class FinalbillComponent {
  constructor(private service:projectService){}
  ngOnInit(){
    this.data;
  }
   data(customer_Id:any) {
    
   this.service.getorderbycustomerId(customer_Id).subscribe((data)=>{
     console.log(data);
   })
   }
}


