import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { projectService } from '../project.service';

@Component({
  selector: 'app-e-password',
  templateUrl: './e-password.component.html',
  styleUrls: ['./e-password.component.css']
})
export class EPasswordComponent implements OnInit{

  setPass:any
 
  constructor(private router:Router,private service:projectService){
    this.setPass={
      email_Id:"",
      password:""
    };
  }

  ngOnInit(): void {
    //throw new Error('Method not implemented.');
  }

  //method for updatig password 
   pass(setForm:any){
    this.setPass.email_Id=this.service.resetemail;
    console.log(this.service.resetemail)
    this.setPass.password=setForm.password;
    console.log(setForm.password)
   if(this.service.flag){
      this.service.UpadtePassEmailC(this.setPass).subscribe((data:any) => {
        console.log(data)
        if(data!=0){
          let msz = <HTMLElement>document.getElementById('popmsz');
         msz.innerHTML = 'Password Changed Successfully';
          this.router.navigate(['/login']);
        }
      })
    }
    if(!this.service.flag){
      this.service.UpadtePassEmailF(this.setPass).subscribe((data:any) => {
        console.log(data)
        if(data!=0){
          let msz = <HTMLElement>document.getElementById('popmsz');
         msz.innerHTML = 'Password Changed Successfully';
          this.router.navigate(['/login']);
        }
      })
    }
    // if(this.flag){
    //   let msz = <HTMLElement>document.getElementById('popmsz');
    //      msz.innerHTML = 'Failed to change password';
    // }
    // this.service.newpassword(this.setPass).subscribe((data1: any) => {
    //   alert("hii")
    //   if(data1!=0){
    //     this.router.navigate(['/']);
        
    //   }else{
    //     let msz = <HTMLElement>document.getElementById('popmsz');
    //     msz.innerHTML = 'Failed to change password';
    //   }
    // });
       
  }
}
