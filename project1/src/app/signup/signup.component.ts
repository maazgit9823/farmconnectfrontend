import { Component, OnInit } from '@angular/core';
import { projectService } from '../project.service';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  registrationForm: FormGroup;
  constructor(private service :projectService){
    this.registrationForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl('', [Validators.required]),

    });

  }
onSubmit(customer:any) {
  if (this.registrationForm.valid) {
    this.service.registercustomer(customer).subscribe();
    console.log(this.registrationForm.value);
  } else {
  alert('please enter details to proceed');
    console.log("Form is invalid");
  }
}
minlengthValidator(minlength: number) {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (control.value && control.value.length < minlength) {
      return { minlength: true };
    }
    return null;
  };
}
}

