import { HostListener, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LogComponent } from './log/log.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CustomerhomepageComponent } from './customerhomepage/customerhomepage.component';
import { projectService } from './project.service';
import { SignupfarComponent } from './signupfar/signupfar.component';
import { FinalbillComponent } from './finalbill/finalbill.component';
import { FarmhomepageComponent } from './farmhomepage/farmhomepage.component';
import { FooterComponent } from './footer/footer.component';
import { FarmerprofileComponent } from './farmerprofile/farmerprofile.component';
import { FarmerheaderComponent } from './farmerheader/farmerheader.component';
import { CustomerheaderComponent } from './customerheader/customerheader.component';
import { CustomerprofileComponent } from './customerprofile/customerprofile.component';
import { UpdateprofileComponent } from './updateprofile/updateprofile.component';
import { UpdatefarmprofComponent } from './updatefarmprof/updatefarmprof.component';
import { AddtocartComponent } from './addtocart/addtocart.component';
import { OrderComponent } from './order/order.component';
import { OrderButtonComponent } from './order-button/order-button.component';
import { UploadProductComponent } from './upload-product/upload-product.component';

import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import { TakemailComponent } from './takemail/takemail.component';
import { EmailOtpComponent } from './email-otp/email-otp.component';
import { EPasswordComponent } from './e-password/e-password.component';
import { MobnumberComponent } from './mobnumber/mobnumber.component';
import { TakeotpComponent } from './takeotp/takeotp.component';
import { SetpasswordComponent } from './setpassword/setpassword.component';
// import { AuthConfigModule } from './auth/auth-config.module';



import { CartcheckoutComponent } from './cartcheckout/cartcheckout.component';
import { AuthConfigModule } from './auth/auth-config.module';
import { AdminComponent } from './admin/admin.component';
import { AheadComponent } from './ahead/ahead.component';
import { AdfarComponent } from './adfar/adfar.component';

import { AdproComponent } from './adpro/adpro.component';







@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    SignupComponent,
    HomeComponent,
    LogComponent,
    CustomerhomepageComponent,
    SignupfarComponent,
    AddtocartComponent,
    OrderComponent,
   OrderButtonComponent,
    FarmhomepageComponent,
    FooterComponent,
    FarmerprofileComponent,
    FarmerheaderComponent,
    CustomerheaderComponent,
    CustomerprofileComponent,
    SignupfarComponent,
    UploadProductComponent,

    UpdateprofileComponent,
    UpdatefarmprofComponent,
    ForgotPasswordComponent,
    TakemailComponent,
    EmailOtpComponent,
    EPasswordComponent,
    MobnumberComponent,
    TakeotpComponent,
    SetpasswordComponent,
    CartcheckoutComponent,
    AdminComponent,
    AheadComponent,
    AdfarComponent,
    AdproComponent,
   
  

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AuthConfigModule
  ],
  providers: [projectService,CustomerhomepageComponent,FarmhomepageComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
