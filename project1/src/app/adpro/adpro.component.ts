import { Component } from '@angular/core';
import { projectService } from '../project.service';

@Component({
  selector: 'app-adpro',
  templateUrl: './adpro.component.html',
  styleUrls: ['./adpro.component.css']
})
export class AdproComponent {
  products:any;
  constructor(private service: projectService){
    this.service.getAllproducts().subscribe((data: any)=>{
      console.log(data);
      this.products = data;
    } )
  }
}
