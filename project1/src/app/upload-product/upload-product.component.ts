import { Component } from '@angular/core';
import { projectService } from '../project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-upload-product',
  templateUrl: './upload-product.component.html',
  styleUrls: ['./upload-product.component.css']
})
export class UploadProductComponent {
  imageUrl: string;
  fileName='';
  reader:FileReader;
  formData: FormData;
  products:any;
  constructor(private service: projectService, private router:Router) {
    this.formData = new FormData();
    this.reader = new FileReader();
    this.imageUrl = '/assets/img/default-image.png';

    this.products={
      product_Id:0,
      product_Name:'',
      product_Price:0,
      product_Desc:'',
      product_Imgsrc:'',
      farmer:{
        farmer_Id: this.service.selctedfarmer.farmer_Id,
        farmer_Name:"",
        farmer_Address:"",
        email_Id:"",
        password:"",
        mobile_Number:"",

      }
    }
  }

  ngOnInit() {
  }

  handleFileInput(event: any) {
    this.formData = new FormData();
    const file:File = event.target.files[0];

    if (file) {

        this.fileName = file.name;


    // // Show image preview
   
    this.reader.onload = (event: any) => {
    this.imageUrl = event.target.result;
     }
    this.reader.readAsDataURL(file);
    this.formData.append('product_imgsrc', file, file.name);
  }
}

  OnSubmit(imageForm: any) {
  
    this.formData.append('product_Name', imageForm.product_Name);
    this.formData.append('product_Desc', imageForm.product_Desc);
    this.formData.append('product_Price', imageForm.product_Price);
    this.formData.append('farmer_Id', this.service.selctedfarmer.farmer_Id);

   this.service.postFile(this.formData).subscribe(
     data => {
        this.router.navigate(['farmhomepage']);
     }
   );
  }
}
