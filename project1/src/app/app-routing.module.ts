import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { LogComponent } from './log/log.component';
import { SignupfarComponent } from './signupfar/signupfar.component';
import { CustomerhomepageComponent } from './customerhomepage/customerhomepage.component';
import { OrderComponent } from './order/order.component';
import { AddtocartComponent } from './addtocart/addtocart.component';
import { FarmhomepageComponent } from './farmhomepage/farmhomepage.component';
import { FarmerprofileComponent } from './farmerprofile/farmerprofile.component';
import { CustomerprofileComponent } from './customerprofile/customerprofile.component';
import { UpdateprofileComponent } from './updateprofile/updateprofile.component';
import { UpdatefarmprofComponent } from './updatefarmprof/updatefarmprof.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { TakemailComponent } from './takemail/takemail.component';
import { EmailOtpComponent } from './email-otp/email-otp.component';
import { EPasswordComponent } from './e-password/e-password.component';
import { MobnumberComponent } from './mobnumber/mobnumber.component';
import { TakeotpComponent } from './takeotp/takeotp.component';
import { SetpasswordComponent } from './setpassword/setpassword.component';
import { AdfarComponent } from './adfar/adfar.component';
import { AdproComponent } from './adpro/adpro.component';
import { AheadComponent } from './ahead/ahead.component';
import { AdminComponent } from './admin/admin.component';
const routes: Routes = [{path:'',component:HomeComponent},
                          {path:'home',component:HomeComponent},
                          {path:'signupfar',component:SignupfarComponent},
                         {path:'login',component:LoginComponent},
                         {path:'login/forgotpassword/takemail/eotp/epassword/login',component:LoginComponent},
                         {path:'signup',component:SignupComponent},
                          {path:'farmhomepage',component:FarmhomepageComponent},
                           {path:'customerhomepage',component:CustomerhomepageComponent},
                           {path:'addtocart',component:AddtocartComponent},
                           {path:'order',component:OrderComponent},
                           {path:'farmhomepage/farmerprofile',component:FarmerprofileComponent},
                           {path:'farmhomepage/updatefarmprof',component:UpdatefarmprofComponent},
                           {path:'customerhomepage/customerprofile',component:CustomerprofileComponent},
                           {path:'customerhomepage/addtocart',component:AddtocartComponent},
                           {path:'customerhomepage/addtocart/order',component:OrderComponent},
                           {path:'customerhomepage/order',component:OrderComponent},
                           {path:'customerhomepage/updateprofile',component:UpdateprofileComponent},
                           {path: 'login/forgotpassword',component:ForgotPasswordComponent},
                           {path:'login/forgotpassword/takemail',component:TakemailComponent},
                           {path:'login/forgotpassword/takemail/eotp',component:EmailOtpComponent},
                           {path:'login/forgotpassword/takemail/eotp/epassword',component:EPasswordComponent},
                           {path:'login/forgotpassword/mob',component:MobnumberComponent},
                           {path:'login/forgotpassword/takemail/takeotp',component:TakeotpComponent},
                           {path:'login/forgotpassword/takemail/takeotp/setpassword',component:SetpasswordComponent},
                           {path:'admin/adfar',component:AdfarComponent},
                           {path:'admin/adpro',component:AdproComponent},
                           {path:'header',component:AheadComponent},
                           {path:'admin',component:AdminComponent},
                           {path:'customerprofile',component:CustomerprofileComponent},
                           {path:'updateprofile',component:UpdateprofileComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
