import { Component,ViewEncapsulation } from '@angular/core';
import { ElementRef,ViewChild } from '@angular/core';

@Component({
  selector: 'app-payment',
  templateUrl: './order-button.component.html',
  // styleUrls: ['./style.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class OrderButtonComponent {
  constructor(){}
  
    @ViewChild('orderButton')
  orderButton!: ElementRef;
  onClick(){
  const button = this.orderButton.nativeElement;
if(!button.classList.contains('animate')){
button.classList.add('animate');
setTimeout(() => {
button.classList.remove('animate');
}, 10000);
    }
  }
}
