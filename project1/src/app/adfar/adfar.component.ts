import { Component } from '@angular/core';
import { projectService } from '../project.service';

@Component({
  selector: 'app-adfar',
  templateUrl: './adfar.component.html',
  styleUrls: ['./adfar.component.css']
})
export class AdfarComponent {
  farmers:any;
  constructor(private service: projectService){
    this.service.getfarmers().subscribe((data: any)=>{
      console.log(data);
      this.farmers = data;
    } )
  }
}
