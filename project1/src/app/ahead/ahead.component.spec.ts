import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AheadComponent } from './ahead.component';

describe('AheadComponent', () => {
  let component: AheadComponent;
  let fixture: ComponentFixture<AheadComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AheadComponent]
    });
    fixture = TestBed.createComponent(AheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
