import { Component, OnInit } from '@angular/core';

import { projectService } from '../project.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  currentIndex = 0;
  cardContainers!: NodeListOf<Element>; 
  products: any[] = []; 
  isAuthenticated: boolean = false;
  product:any;
  constructor(private router: Router,private service: projectService){

  }

  ngOnInit() {
    this.cardContainers = document.querySelectorAll('.card-container');
    this.updateButtonStates();
    this.service.getAllproducts().subscribe((data:any)=>{
      this.product= data;
    });
    // this.products = [
    //   {
    //     name: 'Product 1',
    //     description: 'Description 1',
    //     imageUrl: 'path/to/image1.jpg'
    //   },
    //   {
    //     name: 'Product 2',
    //     description: 'Description 2',
    //     imageUrl: 'path/to/image2.jpg'
    //   },
      // Add more products here
    //];
  }

  moveCarousel(direction: string) {
    if (direction === 'prev') {
      this.currentIndex = Math.max(this.currentIndex - 1, 0);
    } else {
      this.currentIndex = Math.min(this.currentIndex + 1, this.cardContainers.length - 3);
    }
    const carousel = document.querySelector('.carousel') as HTMLElement;
    carousel.style.transform = `translateX(-${this.currentIndex * (100 / 3)}%)`;

    this.updateButtonStates();
  }

  updateButtonStates() {
    const prevButton = document.querySelector('.prev-button') as HTMLElement;
    const nextButton = document.querySelector('.next-button') as HTMLElement;
    prevButton.classList.toggle('inactive', this.currentIndex === 0);
    nextButton.classList.toggle('inactive', this.currentIndex === this.cardContainers.length - 3);
  }


}
