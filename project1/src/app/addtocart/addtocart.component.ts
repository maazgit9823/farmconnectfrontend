import { Component } from '@angular/core';
import { projectService } from '../project.service';
import { Router } from '@angular/router';
import { Observable, isObservable } from 'rxjs';
interface Product {
  product_Id: any;
  product_Name: string;
  product_Price: number;
}

@Component({
  selector: 'app-addtocart',
  templateUrl: './addtocart.component.html',
  styleUrls: ['./addtocart.component.css']
})

export class AddtocartComponent {
  cartData: any[] = [];
  orders:any| undefined;
value: number | undefined;
rzp1:any;
amount:any;
  constructor(private productservice: projectService, private router: Router) {
    this.orders;

   }

   ngOnInit() {
  // this.productservice.cartitems
  //  console.warn(this.cartData)
  //  this.productservice.getitems;
  // this.productservice.getcart;
this.cartData = this.productservice.cartitems 
this.productservice.value$.subscribe((value) => {
  this.value = value;
});   
}
decreaseValue() {
  this.productservice.decreaseValue();
}

//woring code.
//   removeToCart(cartId:number|undefined){
//     cartId && this.cartData && this.productservice.removeToCart(cartId)
//     .subscribe((result:any)=>{
//       this.cartData =this.productservice.cartitems;
//     })

// alert("remove clicked");
//   }
removeFromCart(product: any) {
  this.productservice.removeProduct(product);
  this.cartData = this.productservice.cartitems;
}

 get total() {
  return  this.cartData?.reduce(
    (sum: { price: number; },product: { quantity: number; product_Price: number; }) => ({
      quantity: 1,
      price: sum.price + product.quantity * product.product_Price,
    }),
    { quantity: 1, price: 0 }
  ).price;
  
}
checkout(product:any){
}
orderdata(product:any){
this.productservice.orderdata = product;
console.log(this .productservice.orderdata);
this.orders={
customer_Id: this.productservice.selectedcustomer.customer_Id,
product_Id:this.productservice.orderdata.product_Id,
quantity: product.quantity,
total:this.total,
order_Type:"NAN",
productimg:this.productservice.orderdata.product_imgsrc,
product_Name: this.productservice.orderdata.product_Name,
customer_Name:this.productservice.selectedcustomer.customer_Name,
mobile_Number:this.productservice.selectedcustomer.mobile_Number,
address:this.productservice.selectedcustomer.customer_address,
}

 this.productservice.uploadorders(this.orders).subscribe((data:any)=>{
  console.log(data); 
  // alert("orderupdated");
  console.warn(this.orders);
  this.productservice.orderitems.push(this.orders);
});
}
//------------------------------------------------------
//paymentcode

 options = {
  "key": "rzp_test_nAsEYQXHCGW4rG", 
  "amount": "10000",
  "currency": "INR",
  "description": "Farmconnect",
  "image": "https://s3.amazonaws.com/rzp-mobile/images/rzp.jpg",
  "prefill":
  {
    "email": "gaurav.kumar@example.com",
    "contact": +919900000000,
  },
  config: {
    display: {
      blocks: {
        utib: { //name for Axis block
          name: "Pay using Axis Bank",
          instruments: [
            {
              method: "card",
              issuers: ["UTIB"]
            },
            {
              method: "netbanking",
              banks: ["UTIB"]
            },
          ]
        },
        other: { //  name for other block
          name: "Other Payment modes",
          instruments: [
            {
              method: "card",
              issuers: ["ICIC"]
            },
            {
              method: 'netbanking',
            }
          ]
        }
      },
      hide: [
        {
        method: "upi"
        }
      ],
      sequence: ["block.utib", "block.other"],
      preferences: {
        show_default_blocks: false // Should Checkout show its default blocks?
      }
    }
  },
 

 }
 pay(){
this.amount= (this.total*100)
  this.options.amount = this.amount;
  this.rzp1 =  new this.productservice.nativeWindow.Razorpay(this.options);

  this.rzp1.open();
 this.finabill();
  }
  finabill(){
// this.router.navigate(["/"]);
  }
  }
