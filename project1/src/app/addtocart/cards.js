const carousel = document.querySelector('.carousel');
const cardContainers = document.querySelectorAll('.card-container');
const prevButton = document.querySelector('.prev-button');
const nextButton = document.querySelector('.next-button');

let currentIndex = 0;

function moveCarousel(direction) {
if (direction === 'prev') {
currentIndex = Math.max(currentIndex - 1, 0);
} else {
currentIndex = Math.min(currentIndex + 1, cardContainers.length - 3);
}
carousel.style.transform = `translateX(-${currentIndex * (100 / 3)}%)`;

updateButtonStates();
}

function updateButtonStates() {
prevButton.classList.toggle('inactive', currentIndex === 0);
nextButton.classList.toggle('inactive', currentIndex === cardContainers.length - 3);
}

prevButton.addEventListener('click', () => moveCarousel('prev'));
nextButton.addEventListener('click', () => moveCarousel('next'));

updateButtonStates();

