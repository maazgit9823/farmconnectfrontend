import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { projectService } from '../project.service';

@Component({
  selector: 'app-email-otp',
  templateUrl: './email-otp.component.html',
  styleUrls: ['./email-otp.component.css'],
})

export class EmailOtpComponent implements OnInit {

  constructor(private router: Router,private service : projectService) {}

  ngOnInit(): void {
    //throw new Error('Method not implemented.');
  }

  // method for taking password from the user
  confirmpass(otpForm: any) {
    if(this.service.otp==otpForm.otp){
      alert("Authorized Otp")
      this.router.navigate(['login/forgotpassword/takemail/eotp/epassword']);
    }
    
  }
  
}
