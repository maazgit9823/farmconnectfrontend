import { Component } from '@angular/core';
import { projectService } from '../project.service';

@Component({
  selector: 'app-farmerheader',
  templateUrl: './farmerheader.component.html',
  styleUrls: ['./farmerheader.component.css']
})
export class FarmerheaderComponent {
  farmer:any;
  editfarmer:any;
  products :any;
  product:any;
  constructor(private service :projectService){
    this.farmer = this.service.selctedfarmer;
    
    this.editfarmer = {farmer_Name: '',mobile_Number: '', farmer_Address:''};

    this.products={
      product_Id:0,
      product_Name:'',
      product_Price:0,
      product_Desc:'',
      product_Imgsrc:'',
      farmer:{
        farmer_Id: this.farmer.farmer_Id,
        farmer_Name:"",
        farmer_Address:"",
        email_Id:"",
        password:"",
        mobile_Number:"",

      }
    }
  }
  modalVisible: boolean = false;
  openModal() {
    this.modalVisible = true;
    console.log("hi this is open");
  }

  closeModal(product:any) {
    this.modalVisible = false;
    console.log("hi this is close");
    this.service.registerproduct(this.products).subscribe();
    console.log(this.products);
    this.service.farmerproduct.push(this.products);
    console.log("product added in database");
  }
// addproduct(product:any){
//   this.service.registerproduct(product).subscribe();
//   console.log("product added");
// }
showedit(editfarmer:any){ 
this.editfarmer = this.farmer;
}
 closeModalclose(){
  this.modalVisible = false;
 }
}
