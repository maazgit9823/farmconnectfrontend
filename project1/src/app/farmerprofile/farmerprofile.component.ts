import { Component } from '@angular/core';
import { projectService } from '../project.service';

@Component({
  selector: 'app-farmerprofile',
  templateUrl: './farmerprofile.component.html',
  styleUrls: ['./farmerprofile.component.css']
})
export class FarmerprofileComponent {
  farmer:any;
  editfarmer:any;
  name:any;
  email:any;
  mobile:any;
  address:any;
  constructor(private service :projectService){
    this.farmer = this.service.selctedfarmer;
    console.log(this.farmer);
    this.editfarmer = {farmer_Name: '',mobile_Number: '', farmer_Address:'', email_Id:''};
   }
   showedit(){
    this.editfarmer = this.farmer;
   }
   updatefarmer(farmer:any){
   
    this.service.updatefarmer(this.editfarmer).subscribe();

  }

}